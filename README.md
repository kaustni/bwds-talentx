## TalentX related files

In the file TalentX_ProgrammingTask there is the advertisement and instructions for the programming task. We could maybe print 3 of these files to the stand, one as advertisement taped on the side of the table and two as instructions on the table.

# Step-by-step solving instructions (for any computer)

1) git clone https://github.com/espressif/esp-idf.git

2) go to the cloned directory and set the variable IDF_PATH to current directory

3) cd examples/bluetooth/ble_ibeacon

4) find the correct port by running ls /dev/cu.* (the port should be something like '/dev/cu.wchusbserial1410')

5) run: 'make menuconfig' and there choose 'Serial Flasher Config' and there change the 'default serial port' to the port that was fount in previous step

6) save and exit the config menu

7) edit set the variable IBEACON_MODE to IBEACON_RECEIVER in file main/esp_ibeacon_api.h (THIS IS THE ACTUAL TASK)

8) run 'make' and press enter to any questions it might ask (this might take some time)

9) make flash

10) make monitor

11) now you should see in the monitor view the following print:

I (19452) IBEACON_DEMO: ----------iBeacon Found----------
I (19452) IBEACON_DEMO: Device address:: d6 89 57 3b dc 9a 
I (19452) IBEACON_DEMO: Proximity UUID:: 11 11 11 11 11 11 11 11 11 12 21 11 11 11 11 11 
I (19462) IBEACON_DEMO: Major: 0x1111 (4369)
I (19472) IBEACON_DEMO: Minor: 0x1111 (4369)
I (19472) IBEACON_DEMO: Measured power (RSSI at a 1m distance):10 dbm
I (19482) IBEACON_DEMO: RSSI of packet:-55 dbm

12) exit the monitor view by pressing ctrl + swedish o


to make some 'make's above run faster, you can parallelize it using 'make -j5' instead of simple 'make'.

If you use Pihla's computer, the steps 1-6 are already done for you.